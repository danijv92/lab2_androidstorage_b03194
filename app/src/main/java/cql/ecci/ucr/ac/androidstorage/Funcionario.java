package cql.ecci.ucr.ac.androidstorage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;

import java.util.Date;

public class Funcionario extends Persona {
    private int unidadBase;
    private int puestoBase;
    private double salarioBase;

    public Funcionario(String identificacion, String correo, String nombre, String primerApellido, String segundoApellido, String telefono, String celular, Date fechaNacimiento, String tipo, String genero, int unidadBase, int puestoBase, double salarioBase) {
        super(identificacion, correo, nombre, primerApellido, segundoApellido, telefono, celular, fechaNacimiento, tipo, genero);
        this.setUnidadBase(unidadBase);
        this.setPuestoBase(puestoBase);
        this.setSalarioBase(salarioBase);
    }

    public int getUnidadBase() {
        return unidadBase;
    }

    public void setUnidadBase(int unidadBase) {
        this.unidadBase = unidadBase;
    }

    public int getPuestoBase() {
        return puestoBase;
    }

    public void setPuestoBase(int puestoBase) {
        this.puestoBase = puestoBase;
    }

    public double getSalarioBase() {
        return salarioBase;
    }

    public void setSalarioBase(double salarioBase) {
        this.salarioBase = salarioBase;
    }

    public Funcionario(Parcel in, int unidadBase, int puestoBase, double salarioBase) {
        super(in);
        this.unidadBase = unidadBase;
        this.puestoBase = puestoBase;
        this.salarioBase = salarioBase;
    }

    // insertar un funcionario en la base de datos
    public long insertar(Context context) {
        // inserta la persona antes del funcionario
        long newRowId = super.insertar(context);

        // si inserta la Persona inserto el funcionario
        if (newRowId > 0) {
            // usar la clase DataBaseHelper para realizar la operacion de insertar
            DataBaseHelper dataBaseHelper = new DataBaseHelper(context);

            // Obtiene la base de datos en modo escritura
            SQLiteDatabase db = dataBaseHelper.getWritableDatabase();

            // Crear un mapa de valores donde las columnas son las llaves
            ContentValues values = new ContentValues();
            values.put(DataBaseContract.DataBaseEntry._ID, getIdentificacion());
            values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_UNIDAD_BASE, getUnidadBase());
            values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_PUESTO_BASE, getPuestoBase());
            values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_SALARIO_BASE, getSalarioBase());

            // Insertar la nueva fila
            newRowId = db.insert(DataBaseContract.DataBaseEntry.TABLE_NAME_FUNCIONARIO, null, values);
        }
        return newRowId;
    }

    // leer un funcionario desde la base de datos
    public void leer (Context context, String identificacion){
        // leer la persona antes del funcionario
        super.leer(context, identificacion);

        // si lee a la persona, leo el funcionario
        if (getTipo().equals(Persona.TIPO_ADMINISTRATIVO) || getTipo().equals(Persona.TIPO_PROFESOR)) {
            // usar la clase DataBaseHelper para realizar la operacion de select
            DataBaseHelper dataBaseHelper = new DataBaseHelper(context);

            // Obtiene la base de datos en modo lectura
            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            // Define cuales columnas quiere solicitar // en este caso todas las de la clase
            String[] projection = {
                    DataBaseContract.DataBaseEntry._ID,
                    DataBaseContract.DataBaseEntry.COLUMN_NAME_UNIDAD_BASE,
                    DataBaseContract.DataBaseEntry.COLUMN_NAME_PUESTO_BASE,
                    DataBaseContract.DataBaseEntry.COLUMN_NAME_SALARIO_BASE};

            // Filtro para el WHERE
            String selection = DataBaseContract.DataBaseEntry._ID + " = ?";
            String[] selectionArgs = {identificacion};

            // Resultados en el cursor
            Cursor cursor = db.query(
                    DataBaseContract.DataBaseEntry.TABLE_NAME_FUNCIONARIO, // tabla
                    projection,                                        // columnas
                    selection,                                         // where
                    selectionArgs,                                     // valores del where
                    null,                                              // agrupamiento
                    null,                                              // filtros por grupo
                    null                                               // orden
            );

            // recorrer los resultados y asignarlos a la clase
            if (cursor.moveToFirst() && cursor.getCount() > 0) {
                setUnidadBase(cursor.getInt(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_UNIDAD_BASE)));
                setPuestoBase(cursor.getInt(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_PUESTO_BASE)));
                setSalarioBase(cursor.getDouble(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_SALARIO_BASE)));
            }
        }
    }

    // eliminar un funcionario desde la base de datos
    public void eliminar (Context context, String identificacion) {
        // usar la clase DataBaseHelper para realizar la operacion de eliminar
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);

        // Obtiene la base de datos en modo escritura
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();

        // Define el where para el borrado
        String selection = DataBaseContract.DataBaseEntry._ID + " LIKE ?";

        // Se detallan los argumentos
        String[] selectionArgs = {identificacion};

        // Realiza el SQL de borrado
        db.delete(DataBaseContract.DataBaseEntry.TABLE_NAME_FUNCIONARIO, selection, selectionArgs);

        // eliminar la persona despues del funcionario
        super.eliminar(context, identificacion);
    }

    // actualizar un funcionario en la base de datos
    public int actualizar(Context context) {
        // usar la clase DataBaseHelper para realizar la operacion de actualizar
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);

        // Obtiene la base de datos
        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

        // Crear un mapa de valores para los datos a actualizar
        ContentValues values = new ContentValues();
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_UNIDAD_BASE, getUnidadBase());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_PUESTO_BASE, getPuestoBase());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_SALARIO_BASE, getSalarioBase());

        // Criterio de actualizacion
        String selection = DataBaseContract.DataBaseEntry._ID + " LIKE ?";

        // Se detallan los argumentos
        String[] selectionArgs = {getIdentificacion()};

        // Actualizar la base de datos
        int contador = db.update(DataBaseContract.DataBaseEntry.TABLE_NAME_FUNCIONARIO, values, selection, selectionArgs);

        // actualizar la persona despues del funcionario
        contador += super.actualizar(context);

        return contador;
    }
}
